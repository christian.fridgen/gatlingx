This is a collection of various gattling tests.
To point your local gattling installation to this directory, you have to manually edit gattling.conf and point the simulations and the resources directory to the corresponding subdirectories of this project.

Some hints for setting this up:
 - Install the scala Plugin in IntelliJ. I haven't managed to get the tests to compile and run inside IntelliJ, but it still helps to have Syntax completion for Scala
 - In your local gatling config directory there is a file logback.xml. Add/Uncomment this line 
```
<!-- uncomment and set to DEBUG to log all failing HTTP requests -->
<!-- uncomment and set to TRACE to log all HTTP requests -->
<logger name="io.gatling.http.engine.response" level="ERROR" />
```
 to get debugging of your calls
 - There is a shell script included g.sh which either runs a specific test (needs a fully qualified test name like basic.GetAccountTest) or provides a test selection just like running gatling normally. I recommend to put this script into your path (e.g. /usr/local/bin)
 - If you do not want to enter a password in each and every test, create a file src/test/resources/loginRequest.txt with valid credentials. This file is in .gitignore to not accidentally put it into git. Look at loginRequest_sample.txt for the format. The file gatling_runner.txt is not used (yet)
 - At the beginnign of each test is a Map with configuration variables. This can be used to define different configurations for different environments. Put the one you want to use inti the variable config as this is the one that will be referred to in the tests
 - Package basic contains basic building blocks (create accont, create user etc.) and can easily be used as templates for new domain objects
 - Package combined is desinged to contain more complex tests (like create account, create user, create membership, create identifier, and create some bookings) in one single test

Open Issues:
 - Currently each test set contains a login that is part of the test suite. As the login itself is relatively slow compared to all other calls, this dilutes the test results regarding perfomance. To get better results, this login should be moved out of the repetition loop (and could even be defined as warmup-test in gatling) and compare only "real" tests
 - Some tests still include a replace of http by https when receiving an URL from the location header. The URL in the location header is always returned as http and any call to http will be redirected to the https call. This breaks the Auth Token in gatling, the Auth Token will not be re-sent on redirects (by design).
    - To solve this correctly, the Auth Header shoult be set on protocol level. This doesn't work right now as the evaluation happens even before the login call where the auth token is retrieved. This is up to someone more proficient in scala how the immutable protocoll header can be changed...
    - The current approach is to just split the newly generated ID from the location header (it is often required for other calls too) and build the new URL in the test. Not really HATEOAS way, but works
  

 
 
 

                                                                                        	

