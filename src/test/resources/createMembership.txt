{
  "accountId": "${accountid}",
  "userId": "${userid}",
  "memberRole": "OWNER",
  "optins": [
    {
      "type": "email",
      "flag": true
    }
  ]
}