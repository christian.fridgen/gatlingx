{
  "managingUnit": {
    "unitType": "Client",
    "value": 1
  },
  "usageType": "Incentive",
  "externalReference": "ext_1",
  "externalCode": {
  	"value":"Ext_Text",
  	"type": "TEXT"
  },
  "sponsor": {
    "unitType": "Client",
    "value": 1
  },
  "conditions": [
    {
      "conditionType": "effectiveness",
      "conditionUsage": "REDEEM_VISIBILITY",
      "propertyPath": "system.currentDateTime",
      "operator": ">=",
      "value": "2019-03-01T00:00:00"
    }
  ],
    "redeemActions": [
    {
      "actionType": "FixedPointAction",
      "value": 100
    }
  ],
"customProperties": [
    {
      "name": "Redeem_Channel",
      "value": "online"
    },
    {
      "name": "SortOrder",
      "value": 10
    }
  ],
  "i18nFields": {
    "de": {
      "title": "de-title-${dynamic_couponname}",
      "subTitle": "de-subtitle-${dynamic_couponname}",
      "description": "de-desc-${dynamic_couponname}"
    },
    "en": {
      "title": "en-title-${dynamic_couponname}",
      "subTitle": "en-subtitle-${dynamic_couponname}"
    }
  }
}