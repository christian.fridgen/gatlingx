package default

import scala.concurrent.duration._
import scala.util.Random

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import io.gatling.core.session.Expression


class Arts2pfmRepeater extends Simulation {

	val usersInParallel = 2
	val repetitionPerUser = 2

	val config_ocmtest = Map(
		"headers" -> Map(
			"API-Key" -> "18cf32059682e585108acd5479ef2c0c2fd610dffb5665faf3b94e0f0491dbc6",
			"cache-control" -> "no-cache",
			"Content-TYpe" -> "application/x-www-form-urlencoded"
		),
		"baseurl" -> "https://ocm.test.point4more.com/",
		"path" -> "/arts",
		"expected" -> 201
	)

	val config_test = Map(
		"headers" -> Map(
			"API-Key" -> "703afe21a16a29c6f5de8b3d5a5a06c7fc08d758ec95dea083320067682bd11d",
			"cache-control" -> "no-cache"
		),
		"baseurl" -> "http://test.point4more.com/",
		"path" -> "/api/Arts",
		"expected" -> 200
	)

	// configure the test setup here!!
	val config = config_ocmtest


	val httpProtocol = http
		.baseUrl(config("baseurl").toString)
		.inferHtmlResources()
		.acceptHeader("*/*")
		.acceptEncodingHeader("gzip, deflate")
		.contentTypeHeader("application/xml")
		.userAgentHeader("GatlingRunner")

	val scn = scenario("arts2pfmRepeater")
		.repeat(repetitionPerUser) {
			exec(_.set("bonid", Random.nextInt(100000000) + 1)) //UUID.randomUUID()))
				.exec(http("dummy")
				.post(config("path").toString())
				.headers(config("headers").asInstanceOf[Map[String,String]])
				.body(ElFileBody("arts2pfm-test.txt"))
				.check(status.is(config("expected").asInstanceOf[Int])))
		}
	setUp(scn.inject(atOnceUsers(usersInParallel))).protocols(httpProtocol)
}