package combined

import java.util.concurrent.TimeUnit

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.util.Random

class CreateCompleteAccountTest extends Simulation {

  val usersInParallel = 2
  val repetitionPerUser = 2
  val bookingsToCreateMin = 5
  val bookingsToCreateMax = 20

  val config_test = Map(
    "headers" -> Map("Authorization" -> "Bearer ${authToken}",
      "Content-Type" -> "application/json",
      "interaction-id" -> "gatling"),
    "baseurl" -> "https://staging.convercus.io",
    // not required for this setup
    // "path" -> "/api/Arts",
    //"expected" -> 200
  )

  // configure the test setup here!!
  val config = config_test


  val httpProtocol = http
    .baseUrl(config("baseurl").toString)

  val scn = scenario("login_test")
    // LogIn
    .repeat(repetitionPerUser) {
      // do a login
      exec(http("login")
        .post("/auth/login")
        .header("Content-Type","application/json")
        .body(ElFileBody("loginRequest.txt"))
        .check(bodyString.saveAs("authToken"))
      )
      // call account create
      .exec(http("create_account")
        .post("/accounts")
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .check(header("Location").saveAs("accounturl"))
        .check(status.is(201))
      )
        .exec(session => session.set("accountid", session("accounturl").as[String].substring(session("accounturl").as[String].lastIndexOf("/")+1)))
        .pause("100",TimeUnit.MILLISECONDS) // TODO: too bad we need this to be reliable :-(
        .exec(http("get_account")
          .get(session => {
            "/accounts/".concat(session("accountid").as[String])
          })
          .headers(config("headers").asInstanceOf[Map[String,String]])
          .check(status.is(200))
        )
        // generate "random" user data
        .exec(_.set("userKey", Random.nextInt(100000000) + 1)) //UUID.randomUUID()))
        // call user create
        .exec(http("create_user")
        .post("/users")
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .body(ElFileBody("createUserStaging.txt"))
        .check(header("Location").saveAs("userurl"))
        .check(status.is(201))
      )
        .exec(session => session.set("userid", session("userurl").as[String].substring(session("userurl").as[String].lastIndexOf("/")+1)))
        // now get the user again
        .pause("100",TimeUnit.MILLISECONDS)  // TODO: too bad we need this to be reliable :-(
        .exec(http("get_user")
          .get(session => {
            "/users/".concat(session("userid").as[String])
          })
          .headers(config("headers").asInstanceOf[Map[String,String]])
          .check(status.is(200))
        )
        // create a membership
        .exec(http("create_membership")
        .post("/accounts/${accountid}/memberships")
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .body(ElFileBody("createMembership.txt"))
        .check(header("Location").saveAs("membershipurl"))
        .check(status.is(201))
      )
        // create an identifier
        .exec(http("create_identifier")
        .post("/accounts/${accountid}/identifiers")
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .body(ElFileBody("createIdentifier.txt"))
        //.check(header("Location").saveAs("identifierurl"))
        .check(status.is(202))
      )
        .repeat(Random.nextInt(bookingsToCreateMax-bookingsToCreateMin) + bookingsToCreateMin) {
          // create a booking
          exec(_.set("booking_amount", Random.nextInt(100) + 1)) //UUID.randomUUID()))
            .exec(http("create_booking")
            .post("/accounts/${accountid}/bookings")
            .headers(config("headers").asInstanceOf[Map[String, String]])
            .body(ElFileBody("createBooking.txt"))
            //.check(header("Location").saveAs("identifierurl"))
            .check(status.is(202))
          )
        }
  }
  setUp(scn.inject(atOnceUsers(usersInParallel))).protocols(httpProtocol)
}