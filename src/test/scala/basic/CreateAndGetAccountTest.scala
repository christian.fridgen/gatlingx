package basic

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import scala.util.Random

class CreateAndGetAccountTest extends Simulation {

  val usersInParallel = 20
  val repetitionPerUser = 50

  val config_test = Map(
  "headers" -> Map("Authorization" -> "Bearer ${authToken}",
    "Content-Type" -> "application/json",
    "interaction-id" -> "gatling"),
    "baseurl" -> "https://staging.convercus.io",
    // not required for this setup
    // "path" -> "/api/Arts",
    //"expected" -> 200
  )

//  val config_custtest = Map(
//    "headers" -> Map(
//
//      "cache-control" -> "no-cache"
//    ),
//    "baseurl" -> "http://test.point4more.com/",
//    "path" -> "/api/Arts",
//    "expected" -> 200
//  )

  // configure the test setup here!!
  val config = config_test


  val httpProtocol = http
    .baseUrl(config("baseurl").toString)

  val scn = scenario("login_test")
    // LogIn
    .repeat(repetitionPerUser) {
      exec(http("login")
        .post("/auth/login")
        .header("Content-Type","application/json")
        .body(ElFileBody("loginRequest.txt"))
        .check(bodyString.saveAs("authToken"))
      )
      .exec(http("create_account")
        .post("/accounts")
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .check(header("Location").saveAs("accounturl"))
        .check(status.is(201))
      )
    .exec(session => session.set("accountid", session("accounturl").as[String].substring(session("accounturl").as[String].lastIndexOf("/")+1)))
      .exec(http("get_account")
        .get(session => {
          "/accounts/".concat(session("accountid").as[String])
        })
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .check(status.is(200))
      )
  }
  setUp(scn.inject(atOnceUsers(usersInParallel))).protocols(httpProtocol)
}