package basic

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.util.Random

class CreateAndGetCouponTest extends Simulation {

  val usersInParallel = 2
  val repetitionPerUser = 3

  val config_test = Map(
  "headers" -> Map("Authorization" -> "Bearer ${authToken}",
    "Content-Type" -> "application/json",
    "interaction-id" -> "gatling"),
    "baseurl" -> "https://staging.convercus.io",
    // not required for this setup
    // "path" -> "/api/Arts",
    //"expected" -> 200
  )

//  val config_custtest = Map(
//    "headers" -> Map(
//
//      "cache-control" -> "no-cache"
//    ),
//    "baseurl" -> "http://test.point4more.com/",
//    "path" -> "/api/Arts",
//    "expected" -> 200
//  )

  // configure the test setup here!!
  val config = config_test


  val httpProtocol = http
    .baseUrl(config("baseurl").toString)

  val scn = scenario("login_test")
    // LogIn
    .repeat(repetitionPerUser) {
      exec(http("login")
        .post("/auth/login")
        .header("Content-Type","application/json")
        .body(ElFileBody("loginRequest.txt"))
        .check(bodyString.saveAs("authToken"))
      )
        .exec(_.set("dynamic_couponname", "gatling_".concat((Random.nextInt(1000) + 1).toString()))) //UUID.randomUUID()))
        .exec(http("create_coupon")
        .post("/coupons")
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .body(ElFileBody("createCoupon.txt"))
        .check(header("Location").saveAs("couponurl"))
        .check(status.is(201))
      )
    .exec(session => session.set("couponid", session("couponurl").as[String].substring(session("couponurl").as[String].lastIndexOf("/")+1)))
      .exec(http("get_coupon")
        .get(session => {
          "/coupons/".concat(session("couponid").as[String])
        })
        .headers(config("headers").asInstanceOf[Map[String,String]])
        .check(status.is(200))
      )
  }
  setUp(scn.inject(atOnceUsers(usersInParallel))).protocols(httpProtocol)
}