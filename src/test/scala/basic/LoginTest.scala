package basic

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class LoginTest extends Simulation {

  val usersInParallel = 2
  val repetitionPerUser = 3

  val config_test = Map(
    "headers" -> Map("Authorization" -> "Bearer ${authToken}",
      "Content-Type" -> "application/json",
      "interaction-id" -> "gatling"),
    "baseurl" -> "https://staging.convercus.io",
    "path" -> "/auth/login",
    "expected" -> 200
  )


  // configure the test setup here!!
  val config = config_test

  val httpProtocol = http
    .baseUrl(config("baseurl").toString())

  val scn = scenario("login_test")
    // LogIn
    .repeat(repetitionPerUser) {
      exec(http("login")
        .post(config("path").toString())
        .header("Content-Type","application/json")
        .body(ElFileBody("loginRequest.txt"))
        .check(bodyString.saveAs("authToken"))
        .check(status.is(config("expected").asInstanceOf[Int]))
      )
    }
  setUp(scn.inject(atOnceUsers(usersInParallel))).protocols(httpProtocol)
}